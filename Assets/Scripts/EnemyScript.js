﻿#pragma strict

// -- Followscript variables
private var player : GameObject;
var moveSpeed: int = 2;
var maxDist : int = 10;
var minDist : int = 0;
// -- End of Followscript variables

var weapons : Component[];
var hasSpawn : boolean;
var moveScript : MoveScript; 

function Awake(){
	weapons = GetComponentsInChildren(WeaponScript) as Component[];
	moveScript = GetComponent(MoveScript);
}

function Update () {

	player = GameObject.FindWithTag("Player");

	if(player) {
		var playerTransform : Transform = player.transform;
		transform.LookAt(playerTransform);
     	transform.Rotate(new Vector3(0,90,0),Space.Self);//correcting the original rotation
     	if (Vector3.Distance(transform.position,playerTransform.position) >= 0){//move if distance from target is greater than 1
       		transform.Translate(new Vector3(-1 * moveSpeed* Time.deltaTime,0,0) );
     	}
	}

	for(var weapon : WeaponScript in weapons){
		if(weapon && weapon.CanAttack() && weapon.enabled){
			weapon.Attack(true);
		}
	}
}

function FixedUpdate() {
		Physics2D.IgnoreLayerCollision(9,10);
		Physics2D.IgnoreLayerCollision(10,10); // don't want enemies hitting each other
}



