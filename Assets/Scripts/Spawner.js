﻿#pragma strict

var spawnTime : float = 5;
var spawnDelay : float = 3;
private static var spawnCount : float = 0;
private static var isSpawning : boolean = false;

var enemies : GameObject[];

function Start () {
	isSpawning = true;
	Spawn();
}

function Spawn () {

	while(isSpawning) {
		yield WaitForSeconds(spawnTime);
		var enemyIndex : int = Random.Range(0, enemies.Length);
		Instantiate(enemies[enemyIndex], transform.position, transform.rotation);
		spawnCount = spawnCount + 1;

		if(spawnCount < 5) {
			spawnTime = 5;
		} else if(spawnCount < 20) {
			spawnTime = 4;
		} else if(spawnCount < 40) {
			spawnTime = 3;
		} else if(spawnCount < 60) {
			spawnTime = 2;
		} else {
			spawnTime = 1;
		}

		if(HUDManager.Instance.GetIsGameOver()) {
			spawnCount = 0;
			isSpawning = false;
		}
	}

}