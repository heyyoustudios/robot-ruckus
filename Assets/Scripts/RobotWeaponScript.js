﻿#pragma strict

var currentWeapon : Transform;
var weapon1 : Transform;
var weapon2 : Transform;
var weapon3 : Transform;


var currentShootingRate : float = 0.25;
var weapon1ShootingRate : float = 0.25;
var weapon2ShootingRate : float = 0.50;
var weapon3ShootingRate : float = 1;

var currentShootCooldown : float;
var weapon1ShootCooldown : float;
var weapon2ShootCooldown : float;
var weapon3ShootCooldown : float;

var isRocket : boolean = false;

function Start () {
	currentWeapon = weapon1;
	currentShootingRate = weapon1ShootingRate;
	weapon1ShootCooldown = 0;
	weapon2ShootCooldown = 0;
	weapon3ShootCooldown = 0;
	currentShootCooldown = 0;
}

function Update () {
	// allows each weapon to cool down, even when not in use
	if(currentShootCooldown > 0){
		currentShootCooldown = currentShootCooldown - Time.deltaTime;
	}
	if(weapon1ShootCooldown > 0){
		weapon1ShootCooldown = weapon1ShootCooldown - Time.deltaTime;
	}
	if(weapon2ShootCooldown > 0){
		weapon2ShootCooldown = weapon2ShootCooldown - Time.deltaTime;
	}
	if(weapon3ShootCooldown > 0){
		weapon3ShootCooldown = weapon3ShootCooldown - Time.deltaTime;
	}
	if (Input.GetKeyDown ("1")){
		changeWeapon(1);
	}
	if (Input.GetKeyDown ("2")){
		changeWeapon(2);
	}
	/*if (Input.GetKeyDown ("3")){
		changeWeapon(3);
	}*/
}

function Attack(isEnemy : boolean){
	if(CanAttack()){
		currentShootCooldown = currentShootingRate;

		if(isRocket) {
			SoundEffectsHelper.Instance.MakeRocketSound();
		} else {
			SoundEffectsHelper.Instance.MakePlayerShotSound();
		}
		var shotTransform : Transform = Instantiate(currentWeapon);
		if(!isEnemy){
			shotTransform.tag = "PlayerShot";
		}
		// give our bullet the point to start from
		shotTransform.position = transform.position;
		// get our position in world coordinates
		var pos = Camera.main.WorldToScreenPoint(transform.position);
		// the vector to where the mouse is 
		var dir = Input.mousePosition - pos;
		// the angle we need to rotate to point at
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		// give our bullet the directon to fire
		shotTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward); 

		var shot : ShotScript = shotTransform.gameObject.GetComponent(ShotScript); 
		if(shot){
			shot.isEnemyShot = isEnemy;
		}

		var move : BulletMoveScript = shotTransform.gameObject.GetComponent(BulletMoveScript); 
		if(move){
			// direction facing the cursor
			var dir2 = new Vector2(dir.x, dir.y);
			move.direction = dir2.normalized;
		}
	}
}

function changeWeapon(weaponChoice){
	switch (weaponChoice) {
		case 1 :
			currentWeapon =  weapon1;
			currentShootCooldown = weapon1ShootCooldown;
			currentShootingRate = weapon1ShootingRate;
			isRocket = false;
		break;
		case 2 : 
			currentWeapon =  weapon2;
			currentShootCooldown = weapon2ShootCooldown;
			currentShootingRate = weapon2ShootingRate;
			isRocket = true;
		break;
		case 3: 
			currentWeapon =  weapon3;
			currentShootCooldown = weapon3ShootCooldown;
			currentShootingRate = weapon3ShootingRate;
			isRocket = false;
		break;
		default:
			currentWeapon =  weapon1;
			currentShootCooldown = weapon1ShootCooldown;
			currentShootingRate = weapon1ShootingRate;
			isRocket = false;
	}
}

function CanAttack(){
	return currentShootCooldown <= 0; 
}