﻿#pragma strict

var shotPrefab : Transform;

var target : Transform; // the "player", only needed by enemies

var shootingRate : float = 0.25;

var shootCooldown : float;

function Start () {
	shootCooldown = 0;
}

function Update () {
	if(shootCooldown > 0){
		shootCooldown = shootCooldown - Time.deltaTime;
	}
}

function Attack(isEnemy : boolean){
	if(CanAttack()){
		shootCooldown = shootingRate;

		if(isEnemy) {
			var player : GameObject = GameObject.FindWithTag("Player");
			

			if(player) {

				target = GameObject.FindWithTag("Player").transform;
				
				var enemyShotTransform : Transform = Instantiate(shotPrefab);
				// give our bullet the point to start from
				enemyShotTransform.position = transform.position;
				// get enemy position in world coordinates
				var enemyPos = Camera.main.WorldToScreenPoint(transform.position);
				// player position
				var playerPos = Camera.main.WorldToScreenPoint(target.position);
				var enemyDir =  playerPos - enemyPos;
				// the angle we need to rotate to point at
				var enemyAngle = Mathf.Atan2(enemyDir.y, enemyDir.x) * Mathf.Rad2Deg;
				// give our bullet the directon to fire
				enemyShotTransform.rotation = Quaternion.AngleAxis(enemyAngle, Vector3.forward); 

				var enemyShot : ShotScript = enemyShotTransform.gameObject.GetComponent(ShotScript); 
				if(enemyShot){
					enemyShot.isEnemyShot = isEnemy;
				}

				var enemyMove : BulletMoveScript = enemyShotTransform.gameObject.GetComponent(BulletMoveScript); 
				if(enemyMove){
					// direction facing the player
					var enemyDir2 : Vector2 = new Vector2(enemyDir.x, enemyDir.y);
					enemyMove.direction = enemyDir2.normalized;
				}
			}
		} 
	}
}

function CanAttack(){
	return shootCooldown <= 0; 
}