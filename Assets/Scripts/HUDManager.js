﻿#pragma strict

public static var Instance : HUDManager;

private static var score : int = 0;
private static var health : int;
private static var isGameOver : boolean;
private static var playerName : String;

public var style : GUIStyle;
public var texture : Texture2D;
private var secretKey="&1+ez}E/Jv7^95y"; // Edit this value and make sure it's the same as the one stored on the server
var addScoreUrl="http://www.robotruckus.heyyoustudios.com/addscore.php?"; //be sure to add a ? to your url
function Awake () {
	if(Instance) {
		Debug.LogError("Multiple instances of SoundEffectsHelper!");
	}
	Instance = this;
}

function Start() {
	isGameOver = false;
  playerName = "ABC";

  texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
  texture.SetPixel(0, 0, new Color(1.0f, 1.0f, 1.0f, 0.5f));
  texture.Apply();
  style.normal.textColor = Color.black;
  style.normal.background = texture;
}

function UpdateScore (newScore : int) {
	score = score + newScore;
}

function GetScore() {
	return score;
}

function ResetScore() {
	score = 0;
}

function UpdateHealth (newHealth : int) {
	health = newHealth;
}

function OnGUI() {
	  GUI.Box (Rect (100,10,110,30), "Time: " + Time.timeSinceLevelLoad.ToString("0"));
  	GUI.Box (Rect (500,10,110,30), "Score: " + score);
  	GUI.Box (Rect (610,10,110,30), "Health: " + health);
  	GUI.Box (Rect (720,10,110,30), "HighScore: " + PlayerPrefs.GetInt("HScore"));

    if(isGameOver) {

        var buttonWidth : int = 120;
        var buttonHeight : int = 60;
        var fnCallBack : Function;
        HUDManager.Instance.SetIsGameOver(true);

        GUI.Label(new Rect((Screen.width/2)-200, 100 - (buttonHeight / 2), 200, 100), "Enter Name: ", style);

        playerName = GUI.TextField(new Rect((Screen.width/2), 100 - (buttonHeight / 2), 200, 100), playerName, 3, style);

        if(GUI.Button(new Rect(Screen.width / 2 - (buttonWidth / 2), (1 * Screen.height / 2) - (buttonHeight / 2), buttonWidth, buttonHeight), "Retry!")) {
          fnCallBack = function(){
            HUDManager.Instance.ResetScore();
            HUDManager.Instance.SetIsGameOver(false);
            Application.LoadLevel("InitialScene");
          };
          HUDManager.Instance.AddScore(playerName, HUDManager.Instance.GetScore(), fnCallBack);
          
        }

        if(GUI.Button(new Rect(Screen.width / 2 - (buttonWidth / 2), (2 * Screen.height / 3) - (buttonHeight / 2), buttonWidth, buttonHeight), "Back to Menu")) {
          fnCallBack = function(){
            HUDManager.Instance.ResetScore();
            HUDManager.Instance.SetIsGameOver(false);
            Application.LoadLevel("Menu");
          };
          HUDManager.Instance.AddScore(playerName, HUDManager.Instance.GetScore(), fnCallBack);
        }
    }
}

function SetIsGameOver(isOver : boolean) {
	isGameOver = isOver;
}

function GetIsGameOver() {
	return isGameOver;
}

function AddScore(name : String, highScore : int, fnCallBack : Function){
   var newScore : int;
   var newName : String;
   var oldScore : int;
   var oldName : String;
   newScore = highScore;
   newName = name;

  if(PlayerPrefs.HasKey("HScore")){
    Debug.Log("Has Prefs");
    Debug.Log("Player Score: " + PlayerPrefs.GetInt("HScore"));
    if(PlayerPrefs.GetInt("HScore") < newScore){ 
      PlayerPrefs.SetInt("HScore", newScore);
      PlayerPrefs.SetString("HScoreName", newName);
      postScore(newName, "" + newScore, fnCallBack);
      return;
    }
  }else{
    Debug.Log("No Prefs");
    PlayerPrefs.SetInt("HScore", newScore);
    PlayerPrefs.SetString("HScoreName", newName);
    postScore(newName, "" + newScore, fnCallBack);
    return;
  }
  fnCallBack();
}

function postScore(name : String, score: String, fnCallBack: Function) {
    //This connects to a server side php script that will add the name and score to a MySQL DB.
    // Supply it with a string representing the players name and the players score.

    var hash = Md5.Md5Sum("" + name + score + secretKey); 
 
    var highscore_url = addScoreUrl + "name=" + WWW.EscapeURL(name) + "&score=" + score + "&hash=" + hash;
 
    // Post the URL to the site and create a download object to get the result.
     
    var hs_post = WWW(highscore_url);
    
    yield hs_post; // Wait until the download is done
  
    if(hs_post.error) {
        Debug.Log("There was an error posting the high score: " + hs_post.error);
    }else{
       fnCallBack();
    }
}
