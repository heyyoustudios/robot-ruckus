﻿#pragma strict

function IsVisibleFrom(renderer: Renderer, camera: Camera){
	var planes : Plane[] = GeometryUtility.CalculateFrustumPlanes(camera);
	return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);  
}