﻿#pragma strict

public static var Instance : SpecialEffectsHelper;

var smokeEffect : ParticleSystem;
var fireEffect : ParticleSystem;
var bigSmokeEffect : ParticleSystem;
var effectsCollider : Transform;

function Awake() {
	if(Instance) {
		Debug.LogError("Multiple instances of SpecialEffectsHelper");
	}

	Instance = this;
}

function BigExplosion(position : Vector3) {
	instantiate(bigSmokeEffect, position);
	var fxCollider = Instantiate(effectsCollider, position, Quaternion.identity);
	fxCollider.tag = "Explosion";
	instantiate(fireEffect, position);
}

function Explosion(position : Vector3) {
	instantiate(smokeEffect, position);

	instantiate(fireEffect, position);
}

function instantiate(prefab : ParticleSystem, position : Vector3 ) {

	var newParticleSystem : ParticleSystem = Instantiate(prefab, position, Quaternion.identity) as ParticleSystem;

	Destroy(newParticleSystem.gameObject, newParticleSystem.startLifetime);

	return newParticleSystem;
}