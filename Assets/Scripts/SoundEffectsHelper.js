﻿#pragma strict

public static var Instance : SoundEffectsHelper;

var explosionSound : AudioClip;
var playerShotSound : AudioClip;
var enemyShotSound : AudioClip;
var rocketSound : AudioClip;

function Awake () {
	if(Instance) {
		Debug.LogError("Multiple instances of SoundEffectsHelper!");
	}
	Instance = this;
}

function MakeExplosionSound() {
	MakeSound(explosionSound);
}

function MakeRocketSound() {
	MakeSound(rocketSound);
}

function MakePlayerShotSound() {
	MakeSound(playerShotSound);
}

function MakeEnemyShotSound() {
	MakeSound(enemyShotSound);
}

function MakeSound(originalClip : AudioClip) {
	AudioSource.PlayClipAtPoint(originalClip, transform.position);
}







