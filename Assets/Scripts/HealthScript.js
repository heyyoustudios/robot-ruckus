﻿#pragma strict
var hp : int = 1;
var isEnemy : boolean = true;

function Start() {
	if(!isEnemy) {
		// If the player, update the health display in the HUD
		HUDManager.Instance.UpdateHealth(hp);
	}
}

function Damage(damageCount : int){
	hp = hp - damageCount;
	if(!isEnemy) {
		// Update health display of the player when damaged
		HUDManager.Instance.UpdateHealth(hp);
	} else {
		// just a default scoring of 20 points per hit at the moment...
		HUDManager.Instance.UpdateScore(20);
	}

	if(hp <= 0){

		// BIG EXPLOSIONS
		if(damageCount > 49){
			SpecialEffectsHelper.Instance.BigExplosion(transform.position);
		// Little Explosions
		}else{
			SpecialEffectsHelper.Instance.Explosion(transform.position);
			SoundEffectsHelper.Instance.MakeExplosionSound();
		}

		Destroy(gameObject);
	}
}


function OnTriggerEnter2D(otherCollider : Collider2D){
	if(otherCollider.gameObject.tag == "Explosion" && isEnemy){
		Damage(1);
	}else{
		var shot : ShotScript = otherCollider.gameObject.GetComponent(ShotScript);
		if(shot){
			if(shot.isEnemyShot !=  isEnemy){
				Damage(shot.damage);
				Destroy(shot.gameObject);
			}
		}
	}
}