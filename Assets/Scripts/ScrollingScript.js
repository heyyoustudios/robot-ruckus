﻿#pragma strict
import System.Collections.Generic;
import System.Linq;

var speed : Vector2 = new Vector2(10,10);
var direction : Vector2 =  new Vector2(-1,0);
var isLinkedToCamera : boolean = false;
var isLooping : boolean = false;

var backgroundPart : List.<Transform>;

function Start () {
	if(isLooping){
		backgroundPart = new List.<Transform>();
		for(var i : int = 0;  i < transform.childCount; i++){
			var child : Transform = transform.GetChild(i);
			if(child.renderer){
				backgroundPart.Add(child);
			} 
		}

		backgroundPart = backgroundPart.OrderBy(
			function(t){
				return t.position.x;
			}
		).ToList();
	}
}

function Update () {
	var movement : Vector3 =  new Vector3(
		speed.x * direction.x, 
		speed.y * direction.y, 
		0);
	movement = movement * Time.deltaTime;
	transform.Translate(movement);

	if(isLinkedToCamera){
		Camera.main.transform.Translate(movement);
	}

	if(isLooping){
		var firstChild : Transform = backgroundPart.FirstOrDefault();
		if(firstChild){
			if(firstChild.position.x < Camera.main.transform.position.x){
				if(firstChild.renderer.isVisible === false){
					var lastChild :Transform= backgroundPart.LastOrDefault();
					var lastPosition : Vector3 = lastChild.transform.position;
					var lastSize : Vector3 = (lastChild.renderer.bounds.max - lastChild.renderer.bounds.min);

					firstChild.position = new Vector3(lastPosition.x + lastSize.x, firstChild.position.y, firstChild.position.z);

					backgroundPart.Remove(firstChild);
					backgroundPart.Add(firstChild);
				}
			}
		}
	}
}