﻿#pragma strict
import SimpleJSON;
var style : GUIStyle;
var texture : Texture2D;
var oHighScores : JSONNode; 
var bGotHighScores : boolean = false;
private var secretKey="&1+ez}E/Jv7^95y"; // Edit this value and make sure it's the same as the one stored on the server
var addScoreUrl="http://www.robotruckus.heyyoustudios.com/addscore.php?"; //be sure to add a ? to your url
var highscoreUrl="http://www.robotruckus.heyyoustudios.com/display.php"; 

function Start () {

  texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
	texture.SetPixel(0, 0, new Color(1.0f, 1.0f, 1.0f, 0.5f));
 	texture.Apply();
	style.normal.textColor = Color.black;
	style.normal.background = texture;
  getScores();

}




function getScores() {
    gameObject.guiText.text = "Loading Scores";
    var hs_get = WWW(highscoreUrl);
    yield hs_get;
 
    if(hs_get.error) {
      print("There was an error getting the high score: " + hs_get.error);
    } else {
        bGotHighScores = true;
        oHighScores = JSON.Parse(hs_get.text); // this is a GUIText that will display the scores in game.
    }
}


function OnGUI() {

  if(bGotHighScores){
  	GUI.backgroundColor = Color.white;

  	var distanceFromTop : int;
    var aHighScores :JSONArray = oHighScores["scores"];
  	for(var i : int = 0; i < aHighScores.Count; i++){

        distanceFromTop = i * 60 + 20;
        GUI.Label(Rect((Screen.width/2) - 155,distanceFromTop,70,60), (i + 1) + ".", style);
        GUI.Label(Rect((Screen.width/2) - 85,distanceFromTop,180,60), aHighScores[i]["name"], style);
        GUI.Label(Rect((Screen.width/2) + 95,distanceFromTop,100,60), "" + aHighScores[i]["score"], style);
        /*if(PlayerPrefs.HasKey(i + "HScore")){
          // new score is higher than the stored score
          distanceFromTop = i * 60 + 20;

          GUI.Label(Rect(750,distanceFromTop,70,60), (i + 1) + ".", style);

          GUI.Label(Rect(820,distanceFromTop,180,60), PlayerPrefs.GetString(i + "HScoreName"), style);
          GUI.Label(Rect(1000,distanceFromTop,100,60), "" + PlayerPrefs.GetInt(i + "HScore"), style);
        }*/
     }

     	if(GUI.Button(new Rect(Screen.width / 2 - (42), distanceFromTop + 200, 120, 60), "Back to Menu")) {
  	 	Application.LoadLevel("Menu");
  	}
  }
}