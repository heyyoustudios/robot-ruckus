﻿#pragma strict

function OnGUI() {
	var buttonWidth : int = 84;
	var buttonHeight : int = 60;

	if(GUI.Button(new Rect(Screen.width / 2 - (buttonWidth / 2), (1 * Screen.height / 2) - (buttonHeight / 2), buttonWidth, buttonHeight), "Play")) {
		Application.LoadLevel("InitialScene");
	}

	if(GUI.Button(new Rect(Screen.width / 2 - (buttonWidth / 2), (2 * Screen.height / 3) - (buttonHeight / 2), buttonWidth, buttonHeight), "Highscores")) {
	 	Application.LoadLevel("Highscores");
	}

}