﻿#pragma strict

var target 		: Transform;
var distance 	: float = 3.0;
var height 		: float = 3.0;
var damping 	: float = 5.0;

function Update () {
	if(target) {
		var wantedPosition : Vector3  = target.TransformPoint(0, height, -distance);
		transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * damping);
	}

	var pos : Vector3 = transform.position;
   	pos.x = Mathf.Clamp(pos.x, 0.5, 39.5);
   	pos.y = Mathf.Clamp(pos.y, -12, 2);
   	transform.position = pos;
}