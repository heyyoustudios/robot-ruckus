﻿#pragma strict
// Script for moving Robot and alerting animator changes
var maxSpeed : float = 10;
var facingRight : boolean = true;
var anim : Animator;

var speed : Vector2 = new Vector2(50,50);
var movement : Vector2;

// for falling animations :D
var grounded : boolean = false;
var groundCheck : Transform;
var groundRadius : float = 0.4;
var whatIsGround : LayerMask;

var maxJumpForce : float = 700;
var minJumpForce :float = 200;
var jumpForce : float = 100;
var currentJumpForce : float = 0;
var jumping = false;
var time : float = 0;
var maxTime : float = 5;

function Start () {
	anim = GetComponent(Animator);
}

function Update(){
	if(Input.GetKeyUp(KeyCode.W)){
		jumping = false;
		currentJumpForce = 0;
		time = 0;
	}
	// if we're on the ground - we can jump
	if(grounded && Input.GetKeyDown(KeyCode.W)){
		anim.SetBool("Ground", false);
		jumping = true;
	}

	var inputX : float = Input.GetAxis("Horizontal");
	var inputY : float = Input.GetAxis("Vertical");

	movement = new Vector2(
		speed.x * inputX,
		speed.y * inputY);

	// Hardcoded boundaries for the first level
	var pos : Vector3 = transform.position;
   	pos.x = Mathf.Clamp(pos.x, -13.8, 55);
   	//pos.y = Mathf.Clamp(pos.y, -10, 10);
   	transform.position = pos;

   	if(transform.position.y <= -20) {
   		var playerFallHealth : HealthScript = this.GetComponent(HealthScript);
		if(playerFallHealth){
			playerFallHealth.Damage(1);
		}
   	}

	var shoot : boolean = Input.GetButtonDown("Fire1");
	shoot |= Input.GetButtonDown("Fire2");

	if(shoot){
		var weapon : RobotWeaponScript = GetComponent(RobotWeaponScript);
		if(weapon){
			weapon.Attack(false);
		}
	}
}

function FixedUpdate () {
	// true or false depending on collider
	grounded =  Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
	var hasUpMomemtum =  (rigidbody2D.velocity.y > 0);
	grounded = grounded && !hasUpMomemtum;
	anim.SetBool("Ground", grounded);
	
	if(jumping) {

		if(currentJumpForce <= maxJumpForce) {

			var jumpForcePerFrame : float = jumpForce * Time.deltaTime;
			// calculate our cumulative jumpforce per frame
			currentJumpForce = currentJumpForce + jumpForcePerFrame;;
			//apply our force per frame
			rigidbody2D.AddForce(new Vector2(0, jumpForcePerFrame));
		}
	}

	// vertical speed 
	anim.SetFloat("vSpeed", rigidbody2D.velocity.y);
	Physics2D.IgnoreLayerCollision(8,9, rigidbody2D.velocity.y > 0);
	Physics2D.IgnoreLayerCollision(8,11);
	var move : float = Input.GetAxis("Horizontal");

	anim.SetFloat("Speed", Mathf.Abs(move));
	rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);
	
	// if moving right but not facing - flip
	if(move > 0 && !facingRight) {
		Flip();
	// if moving left but not facing left - flip
	} else if(move < 0 && facingRight) {
		Flip();
	}
}

// function to flip direction, reusing running frames
function Flip(){
	facingRight = !facingRight;
	var theScale : Vector3  = transform.localScale;
	theScale.x *= -1;
	transform.localScale = theScale;

}

function OnCollisionEnter2D(collision: Collision2D){
	var damagePlayer : boolean = false;
	var enemy : EnemyScript = collision.gameObject.GetComponent(EnemyScript);
	if(enemy){
		var enemyHealth = enemy.GetComponent(HealthScript);
		if(enemyHealth){
			enemyHealth.Damage(enemyHealth.hp);
		}
		damagePlayer = true;
	}

	if(damagePlayer){
		var playerHealth : HealthScript = this.GetComponent(HealthScript);
		if(playerHealth){
			playerHealth.Damage(1);
		}
	}
}

function OnDestroy() {
	// Game Over
	HUDManager.Instance.SetIsGameOver(true);
}