﻿#pragma strict
public static var Instance : DontStopTheMusic = null;

function Awake() {
     if (Instance != null && Instance != this) {
         Destroy(this.gameObject);
         return;
     } else {
         Instance = this;
     }
     DontDestroyOnLoad(this.gameObject);
 }